#ifndef FILE_HPP
#define FILE_HPP

#include <string>
#include <vector>
#include <cstdint>
#include <chrono>

class File
{
public:
    explicit File(const std::string& path);

    std::string readAllText();
    std::vector<char> readAllBytes();

private:
    void _computeSize();

    std::string m_path;
    uintmax_t m_size;
    //std::chrono::time_point<_File_time_clock> m_lastModified;
};

#endif // FILE_HPP
