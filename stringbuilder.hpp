#ifndef STRINGBUILDER_HPP
#define STRINGBUILDER_HPP

#include <string>
#include <sstream>

class StringBuilder
{
private:
    std::wstring privateString;

public:
    StringBuilder() = default;
    StringBuilder(const std::wstring &initialString);
    StringBuilder(std::size_t capacity);
    StringBuilder(const std::wstring &initialString, std::size_t capacity);

    StringBuilder* append(const std::wstring &toAppend);
    template<typename T>
    StringBuilder* append(const T &toAppend)
    {
        privateString += toString(toAppend);
        return this;
    }
    StringBuilder* appendLine();
    StringBuilder* appendLine(const std::wstring &toAppend);
    StringBuilder* insert(std::size_t position, const std::wstring &toInsert);
    template<typename T>
    StringBuilder* insert(std::size_t position, const T &toInsert)
    {
        privateString.insert(position, toString(toInsert));
        return this;
    }
    void setLength(std::size_t newLength);
    StringBuilder* remove(std::size_t start, std::size_t length);
    StringBuilder* replace(const std::wstring &oldString, const std::wstring &newString);
    std::wstring toString(std::size_t start, std::size_t length) const;
    void ensureCapacity(std::size_t minimumCapacity);

    inline std::wstring toString() const
    {
        return privateString;
    }

    inline wchar_t operator[](std::size_t index) const
    {
        return privateString[index];
    }

    inline std::size_t length() const
    {
        return privateString.length();
    }

    inline std::size_t capacity() const
    {
        return privateString.capacity();
    }

    inline std::size_t maxCapacity() const
    {
        return privateString.max_size();
    }

    inline void clear()
    {
        privateString.clear();
    }

private:

};


#endif // STRINGBUILDER_HPP
