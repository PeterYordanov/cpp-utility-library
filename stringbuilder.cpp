#include "stringbuilder.hpp"

StringBuilder::StringBuilder(const std::wstring &initialString)
{
    privateString = initialString;
}

StringBuilder::StringBuilder(std::size_t capacity)
{
    ensureCapacity(capacity);
}

StringBuilder::StringBuilder(const std::wstring &initialString, std::size_t capacity)
{
    privateString = initialString;
    ensureCapacity(capacity);
}

StringBuilder* StringBuilder::append(const std::wstring &toAppend)
{
    privateString += toAppend;
    return this;
}

StringBuilder* StringBuilder::appendLine()
{
    privateString += L"\r\n";
    return this;
}

StringBuilder* StringBuilder::appendLine(const std::wstring &toAppend)
{
    privateString += toAppend + L"\r\n";
    return this;
}

StringBuilder* StringBuilder::insert(std::size_t position, const std::wstring &toInsert)
{
    privateString.insert(position, toInsert);
    return this;
}

std::wstring StringBuilder::toString(std::size_t start, std::size_t length) const
{
    return privateString.substr(start, length);
}

void StringBuilder::setLength(std::size_t newLength)
{
    privateString.resize(newLength);
}

void StringBuilder::ensureCapacity(std::size_t minimumCapacity)
{
    privateString.reserve(minimumCapacity);
}

StringBuilder* StringBuilder::remove(std::size_t start, std::size_t length)
{
    privateString.erase(start, length);
    return this;
}

StringBuilder* StringBuilder::replace(const std::wstring &oldString, const std::wstring &newString)
{
    std::size_t pos = 0;
    while ((pos = privateString.find(oldString, pos)) != std::wstring::npos) {
        privateString.replace(pos, oldString.length(), newString);
        pos += newString.length();
    }
    return this;
}
