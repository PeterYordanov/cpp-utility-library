#ifndef HARDWARE_HPP
#define HARDWARE_HPP

#include "platform.hpp"
#include <vector>

#define XPS_CPU_BRAND_LENGTH 64
#define XPS_CPU_VENDOR_LENGTH XPS_CPU_BRAND_LENGTH
#define XPS_CPU_REGISTERS 4

namespace xps::hardware {

    namespace details {

        typedef struct CacheLevel
        {
            int level;         /** Cache level (L1, L2, L3) */
            int associativity; /** Associativity */
            int lineSize;      /** Cache line size */
            int size;          /** Size of the cache itself */

        public:
            CacheLevel() = default;
            CacheLevel(int level,
                       int associativity,
                       int lineSize,
                       int size) {
                this->level = level;
                this->associativity = associativity;
                this->lineSize = lineSize;
                this->size = size;
            }
        } CacheLevel;

        typedef struct Registers
        {
            std::uint32_t eax;
            std::uint32_t ebx;
            std::uint32_t ecx;
            std::uint32_t edx;
        } Registers;
    }

    namespace memory {
        unsigned long long getRAMkb();
        int	getRAMGB();
        int getPageSize();
    }

    namespace cpu {
        typedef std::vector<details::CacheLevel> CacheLevels;

        std::string getCPUBrandName();
        std::string getVendorName();
        int         getCores();
        int         getLogicalCores();
        CacheLevels getCacheLevels();

        void executeCPUID(details::Registers* regs, unsigned int level);

        bool isSSESupported();
        bool isSSE2Supported();
        bool isSSE3Supported();
        bool isSSE41Supported();
        bool isSSE42Supported();
        bool isAVXSupported();
        bool isAVX2Supported();
        bool isMMXSupported();
        bool isHyperThreadingSupported();
        bool isFPUSupported();
        bool isPCLMULQDQSupported();
    }

    namespace system_info {
        const char* getPlatformName();
        const char* getUserName();
        const char* getHostName();
        bool        is64Bit();
        bool        is32Bit();
        int         getSystemBits();
        bool        isBigEndian();
        bool        isLittleEndian();
        const char* getNativeEndianness();
    }
}

#endif // HARDWARE_HPP
