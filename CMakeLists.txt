cmake_minimum_required(VERSION 3.7...3.18)

if(${CMAKE_VERSION} VERSION_LESS 3.18)
    cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
else()
    cmake_policy(VERSION 3.18)
endif()

project(UtilityLibrary LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(${PROJECT_NAME} SHARED
  baseexception.cpp
  baseexception.hpp
  stringbuilder.cpp
  stringbuilder.hpp
  stringhelpers.cpp
  stringhelpers.hpp
  filesystem.cpp
  filesystem.hpp
  event.cpp
  event.hpp
  unittest.cpp
  unittest.hpp
  stopwatch.cpp
  stopwatch.hpp
  platform.cpp
  platform.hpp
  #hardware.cpp
  #hardware.hpp
  flags.cpp
  flags.hpp
  file.cpp
  file.hpp
  extensions.cpp
  extensions.hpp
  environment_variables.cpp
  environment_variables.hpp
  datetime.cpp
  datetime.hpp
  baseexception.cpp
  baseexception.hpp
)

target_compile_definitions(${PROJECT_NAME} PRIVATE SHAREDLIBRARY_LIBRARY)

