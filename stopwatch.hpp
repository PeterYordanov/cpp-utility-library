#ifndef STOPWATCH_HPP
#define STOPWATCH_HPP

#include <string>
#include <ctime>
#include <chrono>

class StopWatch
{
public:
    StopWatch(const std::string& name)
        : m_name(name)
    {

    }

    void start() {
        m_startTime = std::chrono::system_clock::now();
        m_isRunning = true;
    }

    void stop() {
        m_endTime = std::chrono::system_clock::now();
        m_isRunning = false;
    }

    double elapsedSeconds() {
        return elapsedMilliseconds() / 1000;
    }

    double elapsedMilliseconds() {
        m_endTime = std::chrono::system_clock::now();
        return std::chrono::duration_cast<std::chrono::milliseconds>(m_endTime - m_startTime).count();
    }

private:
    std::string m_name;
    std::chrono::time_point<std::chrono::system_clock> m_startTime;
    std::chrono::time_point<std::chrono::system_clock> m_endTime;
    bool m_isRunning;
};

#endif // STOPWATCH_HPP
