#ifndef EVENT_HPP
#define EVENT_HPP

#include <string>
#include <unordered_map>
#include <vector>
#include <functional>

template<typename T>
class Event
{
public:
    Event() = default;

    void addListener(const std::wstring& methodName, T namedEventHandlerMethod)
    {
        if (m_namedListeners.find(methodName) == m_namedListeners.end())
            m_namedListeners[methodName] = namedEventHandlerMethod;
    }

    void addListener(T unnamedEventHandlerMethod)
    {
        m_anonymousListeners.push_back(unnamedEventHandlerMethod);
    }

    void removeListener(const std::wstring &methodName)
    {
        if (m_namedListeners.find(methodName) != m_namedListeners.end())
            m_namedListeners.erase(methodName);
    }

    std::vector<T> listeners()
    {
        std::vector<T> allListeners;
        for (auto listener : m_namedListeners) {
            allListeners.push_back(listener.second);
        }
        allListeners.insert(allListeners.end(), m_anonymousListeners.begin(), m_anonymousListeners.end());
        return allListeners;
    }

private:
    std::vector<T> m_anonymousListeners;
    std::unordered_map<std::wstring, T> m_namedListeners;
};

#endif // EVENT_HPP
