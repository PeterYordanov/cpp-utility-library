#ifndef PLATFORM_HPP
#define PLATFORM_HPP

#include <initializer_list>
#include <sstream>

#if defined(_WIN32)
    #define XPS_PLATFORM_NAME "Windows" // Windows
    #define XPS_PLATFORM_WINDOWS
#elif defined(_WIN64)
    #define XPS_PLATFORM_NAME "Windows" // Windows
    #define XPS_PLATFORM_WINDOWS
#elif defined(__CYGWIN__) && !defined(_WIN32)
    #define XPS_PLATFORM_NAME "Windows" // Windows (Cygwin POSIX under Microsoft Window)
    #define XPS_PLATFORM_WINDOWS
#elif defined(__ANDROID__)
    #define XPS_PLATFORM_NAME "Android" // Android (implies Linux, so it must come first)
    #define XPS_PLATFORM_ANDROID
#elif defined(__linux__)
    #define XPS_PLATFORM_NAME "Linux" // Debian, Ubuntu, Gentoo, Fedora, openSUSE, RedHat, Centos and other
    #define XPS_PLATFORM_LINUX
#elif defined(__unix__) || !defined(__APPLE__) && defined(__MACH__)
    #include <sys/param.h>
    #if defined(BSD)
        #define XPS_PLATFORM_NAME "BSD" // FreeBSD, NetBSD, OpenBSD, DragonFly BSD
        #define XPS_PLATFORM_BSD
    #endif
#elif defined(__hpux)
    #define XPS_PLATFORM_NAME "HP-UX" // HP-UX
    #define XPS_PLATFORM_HP_UX
#elif defined(_AIX)
    #define XPS_PLATFORM_NAME "AIX" // IBM AIX
    #define XPS_PLATFORM_AIX
#elif defined(__APPLE__) && defined(__MACH__) // Apple OSX and iOS (Darwin)
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR == 1
        #define XPS_PLATFORM_NAME "iOS" // Apple iOS
        #define XPS_PLATFORM_IOS
    #elif TARGET_OS_IPHONE == 1
        #define XPS_PLATFORM_NAME "iOS" // Apple iOS
        #define XPS_PLATFORM_IOS
    #elif TARGET_OS_MAC == 1
        #define XPS_PLATFORM_NAME "OSX" // Apple OSX
        #define XPS_PLATFORM_OSX
    #endif
#elif defined(__sun) && defined(__SVR4)
    #define XPS_PLATFORM_NAME "Solaris" // Oracle Solaris, Open Indiana
    #define XPS_PLATFORM_SOLARIS
#else
    #define XPS_PLATFORM_NAME NULL
#endif

#if __x86_64__ || __ppc64__ || _WIN64
    #define ENV64BIT
#else
    #define ENV32BIT
#endif

#if defined(__LP64__) || defined(_WIN64) || (defined(__x86_64__) && \
    !defined(__ILP32__) ) || defined(_M_X64) || defined(__ia64) || defined (_M_IA64) || defined(__aarch64__) || defined(__powerpc64__)
    #define XPS_IS_64BIT 1
    #define XPS_BITS 64
#else
    #define XPS_IS_32BIT 1
    #define XPS_BITS 32
#endif

#define XPS_DEBUG BUILD_TYPE == "Debug"
#define XPS_RELEASE BUILD_TYPE == "Release"

#define XPS_HIGH_NIBBLE(b) ((b) & (0x0F << 4))
#define XPS_LOW_NIBBLE(b) ((b) & 0x0F)

#define XPS_STR_EXPAND(token) #token

#define XPS_FORCEINLINE inline __attribute__ ((always_inline)))

#define XPS_UNUSED(x) (void)x

#endif // PLATFORM_HPP
