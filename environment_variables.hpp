#ifndef ENVIRONMENT_VARIABLES_HPP
#define ENVIRONMENT_VARIABLES_HPP

#include <cstdlib>

const char* getEnvironmentVariable(const char* variableName) {
    return std::getenv(variableName);
}

#endif // ENVIRONMENT_VARIABLES_HPP
